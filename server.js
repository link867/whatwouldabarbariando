const Discord = require('discord.js');
const client = new Discord.Client();
const auth = require('./.env');

const commands = {
  help: "Shows this help", 
  battlecry: "Gives a barbarian battle cry for battle moral", 
  quote: "Gives a barbarian statement or question", 
  action: "Gives something that a barbarian would (and should) do."
}

let helpInfo = function() {
  let helpOutput = '\n';
  for (var command in commands) {
    helpOutput = `${helpOutput}${command}: ${commands[command]}\n`;
  }
  return helpOutput
}

let getQuote = function() {
  let quoteList = [
    'Fear is a fake emotion, that turns you weak, anger is a real emotion that makes you stronger.',
    'Armor weakens the mind. Let the scars teach you lessons.',
    'I believe in the age old tactic of hitting your enemy harder then they can hit you.',
    'Every man is guided by both emotions and reason. Unlike you, I accept both of them.',
    'Words , no matter how eloquent, have never accomplished anything. They merely moved those who heard them to actual action',
    'I always let mt rage guide my fists, for a blow struck without guidance is the true folly of man',
    'What is best in life? To crush your enemies, see them driven before you, and hear the lamentations of the women.',
    'Good, bad, I\'m the guy with the axe',
    'Come any closer and prepare to meet your ancestors in Hell!',
    'In this place I am a barbarian, because men do not understand me.',
    'We human beings regard ourselves as animals only when it suits us.',
    'You can take the barbarian out of the tavern, but he can take the blood out of your body.',
    '*grunt*',
    'To act in your nature is to be at one with the universe. I\'m simply falling in line.',
  ];

  let randNum = Math.floor(Math.random() * quoteList.length);
  return quoteList[randNum];
}

let getAction = function() {
  let actionList = [
    'Kick the door nearest to you.',
    'Pick up and throw and nearst object.',
    'Grunt at the last action taken by anyone.',
    'Push the person nearest to you. Tell them they can fight you if they don\'t like it.',
    'Complain that it has been too long since you last ate.',
    'Flex your muscles.'
  ];

  let randNum = Math.floor(Math.random() * actionList.length);
  return actionList[randNum];
}


client.on('message', message => {
  if (message.content.match(/^!wwabd/)) {
    let userCommand = message.content.replace("!wwabd ", "");
    if (Object.prototype.hasOwnProperty.call(commands, userCommand)) {
      if (userCommand  === 'battlecry') {
        message.reply('RAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
      }
      else if (userCommand === 'help') {
        message.reply(helpInfo());
      }
      else if (userCommand === 'quote') {
        message.reply(getQuote());
      }
      else if (userCommand === 'action') {
        message.reply(getAction());
      }
    }
    else {
      message.reply(`Invalid command [${userCommand}]. Please use the command 'help' for bot help`);
    }
  }
});

client.login(auth.tokenObject.token);



